class Queue { //изменить именование
    constructor() {
        this.stack1 = [] //изменить именование
        this.stack2 = []
    }

    add(element) {
        this.stack1.unshift(element)
        return this.stack1
    }

    lastElement() {
        this._reverse()
        return this.stack2[this.stack2.length - 1]
    }

    delete() {
        this._reverse()
        return this.stack2.pop()
    }

    _reverse() {
        if(this.stack2.length > 0) {//Вынести в метод
            return
        }
        while (this.stack1.length > 0) {//Вынести в метод
            this.stack2.unshift(this.stack1.pop())
        }
    }
}